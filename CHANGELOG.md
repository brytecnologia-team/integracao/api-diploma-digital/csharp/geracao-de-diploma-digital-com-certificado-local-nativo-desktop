# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-05-31

### Added

- A SIGNATURE generation example. 

[1.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-assinatura/c-sharp/geracao-de-diploma-digital-com-bry-extension/-/tags/1.0.0

## [2.0.0] - 2022-05-10

### Added

- A SIGNATURE generation example. Now includes separate solutions for signing Digital School Records and Public Diploma.

[2.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-assinatura/c-sharp/geracao-de-diploma-digital-com-bry-extension/-/tags/2.0.0