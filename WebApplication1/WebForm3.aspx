﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="WebApplication3.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Exemplo Assinatura Diploma C# - Chave Via Repositório Local</title>
    <link rel="stylesheet" href="Assets/css/bootstrap.min.css">

    <style media="screen">
        .extension-message {
            padding-top: 55px;
            padding-bottom: 55px;
        }

        .btn-extension-install {
            margin-bottom: 55px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div class="container">
            <header class="header clearfix">
                <h2 class="text-muted">
                    <img src="Assets/imgs/logo_bry.png" style="width: 40px;" />
                    Exemplo Assinatura Histórico Digital C# Via Repositório Nativo Windows</h2>
            </header>
            <div id="extensao-instalada">
                <div class="row">
                    <h3>1º Passo:</h3>
                    <p>Insira a Impressão digital do certificado no Repositório Local que deseja utilizar para assinar o Documento.</p>
                    Impressão Digital do Certificado:
                    <br />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <asp:TextBox ID="TextBoxThumbprint" placeholder="Thumbprint Certificado" runat="server" Height="20px" Width="600px"></asp:TextBox><br />
                    <asp:Button class="btn btn-default" ID="Button3" runat="server" OnClick="Get_Certificate" Text="Buscar Certificado" />
                </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                <div class="row">
                    Certificado Dados:
                                <br />

                            <asp:TextBox ID="TextBoxDadosCertificado" placeholder="Dados Certificado" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                            </div>
                <div class="row">
                    <h4>Dados do certificado selecionado:</h4>
                </div>
                            
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nome: </label>
                                        <div class="col-md-6">
                                            <asp:TextBox class="form-control" ID="TextBoxNome" TextMode="SingleLine" placeholder="Nome" ReadOnly="true" runat="server" Height="30px" Width="350px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Emissor: </label>
                                        <div class="col-md-6">
                                            <asp:TextBox class="form-control" ID="TextBoxEmissor" TextMode="SingleLine" placeholder="Emissor" ReadOnly="true" runat="server" Height="30px" Width="350px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Data de validade: </label>
                                        <div class="col-md-6">
                                            <asp:TextBox class="form-control" ID="TextBoxValidade" TextMode="SingleLine" placeholder="Vencimento" ReadOnly="true" runat="server" Height="30px" Width="350px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo de certificado: </label>
                                        <div class="col-md-6">
                                             <asp:TextBox class="form-control" ID="TextBoxTipo" TextMode="SingleLine" placeholder="Tipo Certificado" ReadOnly="true" runat="server" Height="30px" Width="350px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Button3" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <div class="row">
                        <h3>2º Passo:</h3>
                        <p>Escolha qual tipo de assinatura de XML Histórico Escolar Digital será feita e clique no botão baixo para gerar a assinatura.</p>
                        <asp:DropDownList ID="DropDownRole" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="PF: Representante Secretaria" Value="HISTSECRETARIA" />
                        <asp:ListItem Text="PJ: IES Emissora (Envelope)" Value="HISTIESEMISSORA" />
                        </asp:DropDownList>

                    </div>
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4" style="text-align: center;">
                            <asp:Button class="btn btn-lg btn-primary" ID="Button1" runat="server" OnClick="Inicializar_Click" Text="Inicializar Assinatura (Framework)" />
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>

                            <div style="padding-bottom: 5px"></div>
                            <div class="row">
                                Saída Serviço (Inicializar Assinatura)<br />
                                <asp:TextBox ID="TextBoxSaidaInicializar" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                            </div>
                            <div class="row">
                                Entrada Assinatura Chave Privada:<br />
                                <asp:TextBox ID="TextBoxEntradaAssinatura" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Button1" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div style="padding-bottom: 10px"></div>
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4" style="text-align: center;">
                            <asp:Button class="btn btn-lg btn-primary" ID="Button2" runat="server" OnClick="Sign_PrivateKey" Text="Assinar os dados com a Chave Privada Local" />
                        </div>
                    </div>
                    <div class="row">
                        Saída Assinatura Chave Privada:<br />
                        <asp:TextBox ID="TextBoxSaidaAssinatura" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                    </div>
                    <div style="padding-bottom: 10px"></div>
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4" style="text-align: center;">
                            <asp:Button class="btn btn-lg btn-primary" ID="Button4" runat="server" OnClick="Finalize_Click" Text="Finalizar Assinatura (Framework)" />
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <div class="row">
                                Saída Serviço (Finalizar Assinatura)<br />
                                <asp:TextBox ID="TextBoxFinalizar" TextMode="MultiLine" runat="server" Height="60px" Width="1000px"></asp:TextBox>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Button3" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="row">
                    </div>
                </div>
                <!-- end extensao-instalada -->
            </div>
    </form>
    <footer>
        <div class="container">
            <hr>
            <p>&copy;2022 BRy Tecnologia S.A - FSD</p>
        </div>
    </footer>
    <script src="Assets/js/jquery-3.2.1.min.js"></script>
    <script src="Assets/js/bootstrap.min.js"></script>
    <script src="Assets/js/script-customizavel.js"></script>
</body>
</html>
