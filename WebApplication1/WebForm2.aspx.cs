using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Numerics;
using System.Web.Mvc;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        //Gere um token válido em https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insira seu token JWT em INSERT_VALID_ACCESS_TOKEN

        static string INSERT_VALID_ACCESS_TOKEN = "";
        static string NAMESPACE = "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd";
        static string DADOSDIPLOMA = "DadosDiploma";
        static string DADOSREGISTRO = "DadosRegistro";
        static string INCLUDEXPATH = "includeXPathEnveloped";
        //static string DADOSDIPLOMANSF = "DadosDiplomaNSF";
        //static string DADOSREGISTRONSF = "DadosRegistroNSF";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Get_Certificate(object sender, EventArgs e)
        {
            X509Certificate2 certificate = null;

            // Carregar Repositório Nativo do Windows - Repositório do Usuário
            using (X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);
                //Procurando pelo certificado no Repositório por via da Impressão Digital (Hash)
                X509Certificate2Collection certificates = store.Certificates.Find(X509FindType.FindByThumbprint, this.TextBoxThumbprint.Text.ToUpper(), false);

                certificate = certificates[0];
               
                byte[] encodedPublicKey = certificate.RawData;
                byte[] bytes = certificate.GetPublicKey();

                string text = Convert.ToBase64String(encodedPublicKey, Base64FormattingOptions.None);

                this.TextBoxDadosCertificado.Text = text;
                this.TextBoxNome.Text = certificate.FriendlyName;
                this.TextBoxEmissor.Text = certificate.IssuerName.Name;
                this.TextBoxValidade.Text = certificate.NotAfter.ToString();
                char[] separator = { ',' };
                this.TextBoxTipo.Text = certificate.ToString().Split(separator)[2].Trim();

            }
        }

        protected void Sign_PrivateKey(object sender, EventArgs e)
        {
            X509Certificate2 certificate = null;

            RSA privKeySign = null;
            RSACng privKeySignCng = null;

            using (X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);
                //Procurando pelo certificado no Repositório por via da Impressão Digital (Hash)
                X509Certificate2Collection certificates = store.Certificates.Find(X509FindType.FindByThumbprint, this.TextBoxThumbprint.Text.ToUpper(), false);
                certificate = certificates[0];
                privKeySign = certificate.GetRSAPrivateKey();
                privKeySignCng = (RSACng)certificate.GetRSAPrivateKey();
            }
            InputAssinatura input = Deserialize<InputAssinatura>(this.TextBoxEntradaAssinatura.Text);

            byte[][] signatures = new byte[input.assinaturas.Count][];
            
            for (int i = 0; i < input.assinaturas.Count; i++) { 
                using (privKeySign)
                {
                    byte[] data = Convert.FromBase64String(input.assinaturas[i].hashes[0]);

                    SHA256Managed hasher = new SHA256Managed();
                    data = hasher.ComputeHash(data);
                    signatures[i] = privKeySign.SignHash(data, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                
                }
            }

            //Preparação do objeto que guardará as assinaturas
            OutputAssinatura outputEx = new OutputAssinatura();

            List<AssinaturaLocal> listSigs = new List<AssinaturaLocal>();
            for(int i = 0; i < signatures.Length; i++)
            {
                AssinaturaLocal sigs = new AssinaturaLocal();
                List<string> signedHash = new List<string>();
                signedHash.Add(Convert.ToBase64String(signatures[i], Base64FormattingOptions.None));
                sigs.hashes = signedHash;
                sigs.nonce = input.assinaturas[i].nonce;
                listSigs.Add(sigs);
            }

            outputEx.assinaturas = listSigs;
            outputEx.nonce = "123";

            //Serialização para envio ao HUB-FW para Finalização
            this.TextBoxSaidaAssinatura.Text = Serialize<OutputAssinatura>(outputEx);
        }

        protected void Inicializar_Click(object sender, EventArgs e)
        {
            try
            {
                //Preparando os dados para inicialização da assinatura
                string certificateData = this.TextBoxDadosCertificado.Text;
                string tipoAssinatura = this.DropDownRole.SelectedValue;
                this.DropDownRole.Enabled = false;
                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                string responseInitialize = Inicialize(certificateData, tipoAssinatura).Result;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //Desserialização
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                //Caso ocorra erro na desserialização
                if (responseInitializeObj.initializedDocuments == null)
                    throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //Pegando os dados da caixa de texto que é preenchida com retorno do Hub-FW
                this.TextBoxSaidaInicializar.Text = responseInitialize;

                InputAssinatura InputAssinatura = new InputAssinatura();

                InputAssinatura.nonce = responseInitializeObj.nonce;
                InputAssinatura.algoritmoHash = "SHA256";
                InputAssinatura.formatoDadosEntrada = "BASE64";
                InputAssinatura.formatoDadosSaida = "BASE64";

                for (int i = 0; i < responseInitializeObj.signedAttributes.Count; ++i)
                {
                    var input = new AssinaturaLocal();
                    input.nonce = responseInitializeObj.signedAttributes[i].nonce;
                    input.hashes.Add(responseInitializeObj.signedAttributes[i].content);
                    InputAssinatura.assinaturas.Add(input);
                }

                this.TextBoxEntradaAssinatura.Text = Serialize<InputAssinatura>(InputAssinatura);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }

        protected void Finalize_Click(object sender, EventArgs e)
        {
            string certificateData = this.TextBoxDadosCertificado.Text;
            string tipoAssinatura = this.DropDownRole.SelectedValue;
            try
            {
                //Pegando os dados da caixa de texto que é preenchida com retorno do Hub-FW
                String responseInitialize = this.TextBoxSaidaInicializar.Text;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //Desserialização do JSON retornado
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                OutputAssinatura OutputAssinatura = Deserialize<OutputAssinatura>(this.TextBoxSaidaAssinatura.Text);

                //Finalização da assinatura
                string responseFinalize = Finalize(certificateData, OutputAssinatura, responseInitializeObj, tipoAssinatura).Result;
                this.TextBoxFinalizar.Text = responseFinalize;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }

        public async static Task<string> Inicialize(string certificateData, string tipoAssinatura)
        {
            //Aqui montamos a requisição com os dados no formado Multipart
            var requestContent = new MultipartFormDataContent();

            //Define as mudanças na request para cada etapa da assinatura do Diploma
            string tagNodo = "nenhum";

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string nomeArquivo = null;
            switch (tipoAssinatura)
            {
                case "REITOR":
                    tagNodo = "diploma";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "EMISSORA":
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    tagNodo = "diploma";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "PESSOAFISICA":
                    tagNodo = "registro";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
                case "EMISSORAENV": //Caso IES Emissora Arquivamento XML Documentacao Academica
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-documentacao-academica.xml";
                    break;
                case "REGISTRADORADIPL": //Caso IES Registradora XML Diplomado
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-diplomado.xml";
                    break;
                case "HISTSECRETARIA": //Caso PF Rep. Secretaria XML Histórico Escolar
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("ADRT"), "profile");
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    break;
                case "HISTIESEMISSORA": //Caso PJ IES Emissora XML Histórico Escolar
                    tagNodo = "nenhum";
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    break;
            }

            //Demais parâmetros
            requestContent.Add(new StringContent("123"), "nonce");
            requestContent.Add(new StringContent(certificateData), "certificate");
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
            requestContent.Add(new StringContent("ENVELOPED"), "signatureFormat");
            requestContent.Add(new StringContent("LINK"), "returnType");

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./" + nomeArquivo, FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            //É possível adicionar vários documentos aqui, para isto cada documento deve possuir um nonce      
            requestContent.Add(new StringContent("1"), "originalDocuments[0][nonce]");
            requestContent.Add(streamContentDocument, "originalDocuments[0][content]", nomeArquivo);
            //End Loop

            if (tagNodo == "diploma")
            {
                requestContent.Add(new StringContent(DADOSDIPLOMA), "originalDocuments[0][specificNode][name]");
            }
            else if (tagNodo == "registro")
            {
                requestContent.Add(new StringContent(DADOSREGISTRO), "originalDocuments[0][specificNode][name]");
            }

            //Não enviará o Namespace no caso de IES Registradora
            if (tagNodo != "nenhum")
            {
                requestContent.Add(new StringContent(NAMESPACE), "originalDocuments[0][specificNode][namespace]");
            }

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            string result = "";
            try
            {
                var response = await client.PostAsync("https://diploma.bry.com.br/api/xml-signature-service/v2/signatures/initialize", requestContent).ConfigureAwait(false);
                result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            } catch (Exception e)
            {
                Console.WriteLine(e.InnerException.Message);
            }
            // O resultado do BRy Framework é um json com os links
            

            return result;

        }

        public async static Task<string> Finalize(string certificateData, OutputAssinatura OutputAssinatura, ResponseInitialize responseInitializeObj, string tipoAssinatura)
        {
            var requestContent = new MultipartFormDataContent();

            string nomeArquivo = null;

            //Modifica o perfil de assinatura para o da etapa selecionada
            switch (tipoAssinatura)
            {
                case "REITOR":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    break;
                case "EMISSORA":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "PESSOAFISICA":
                    nomeArquivo = "xml-diplomado.xml";
                    requestContent.Add(new StringContent("ADRC"), "profile"); ;
                    break;
                case "EMISSORAENV":
                    nomeArquivo = "xml-documentacao-academica.xml";
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "REGISTRADORADIPL":
                    nomeArquivo = "xml-diplomado.xml";
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
                case "HISTSECRETARIA":
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    requestContent.Add(new StringContent("ADRT"), "profile");
                    break;
                case "HISTIESEMISSORA":
                    nomeArquivo = "xml-historico-escolar.xml"; // Necessario inserir o documento na pasta do exemplo
                    requestContent.Add(new StringContent("ADRA"), "profile");
                    requestContent.Add(new StringContent("false"), INCLUDEXPATH);
                    break;
            }

            //Parâmetros da Request de Finalize
            requestContent.Add(new StringContent(responseInitializeObj.nonce), "nonce");
            requestContent.Add(new StringContent(certificateData), "certificate");
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
            requestContent.Add(new StringContent("ENVELOPED"), "signatureFormat");
            requestContent.Add(new StringContent("SIGNATURE"), "operationType");
            requestContent.Add(new StringContent("LINK"), "returnType");

            //Essa parte pode ser feita em loop para adicionar mais Diplomas para assinatura
            requestContent.Add(new StringContent(OutputAssinatura.assinaturas[0].hashes[0]), "finalizations[0][signatureValue]");
            requestContent.Add(new StringContent(OutputAssinatura.assinaturas[0].nonce), "finalizations[0][nonce]");
            requestContent.Add(new StringContent(responseInitializeObj.initializedDocuments[0].content), "finalizations[0][initializedDocument]");
            //End loop

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./" + nomeArquivo, FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            requestContent.Add(streamContentDocument, "finalizations[0][content]", nomeArquivo);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            var response = await client.PostAsync("https://diploma.bry.com.br/api/xml-signature-service/v2/signatures/finalize ", requestContent).ConfigureAwait(false);

            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }
}

// Data Contracts, utilizados para Serializar e Desserializar JSON Objects

[DataContract]
public class ResponseInitialize
{
    [DataMember]
    public string formatoDadosEntrada;

    [DataMember]
    public string formatoDadosSaida;

    [DataMember]
    public string nonce;

    [DataMember]
    public List<AssinaturasInicializadasIntern> initializedDocuments;

    [DataMember]
    public List<AssinaturasInicializadasIntern> signedAttributes;

    public ResponseInitialize()
    {
        this.initializedDocuments = new List<AssinaturasInicializadasIntern>();
        this.signedAttributes = new List<AssinaturasInicializadasIntern>();
    }
}

[DataContract]
public class AssinaturasInicializadasIntern
{

    [DataMember]
    public string content;

    [DataMember]
    public string nonce;

}

[DataContract]
public class InputAssinatura
{
    [DataMember]
    public string algoritmoHash;

    [DataMember]
    public string nonce;

    [DataMember]
    public string formatoDadosEntrada;

    [DataMember]
    public string formatoDadosSaida;

    [DataMember]
    public List<AssinaturaLocal> assinaturas;

    public InputAssinatura()
    {
        this.assinaturas = new List<AssinaturaLocal>();
    }
}

[DataContract]
public class AssinaturaLocal
{
    [DataMember]
    public string nonce;

    [DataMember]
    public List<string> hashes;

    public AssinaturaLocal()
    {
        this.hashes = new List<string>();
    }
}

[DataContract]
public class OutputAssinatura
{
    [DataMember]
    public string nonce;

    [DataMember]
    public List<AssinaturaLocal> assinaturas;

    public OutputAssinatura()
    {
        this.assinaturas = new List<AssinaturaLocal>();
    }
}
